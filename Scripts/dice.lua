function partyItemDicePanel()
    panel = Panel()
    panel.rect = Rect(Client.width/2+100, 150, 200, 150)
    panel.color = Color(0, 0, 255, 50)
    text1 = Text("주사위", Rect(0, -20, 100, 50))
    aa = math.random(2, 5)

    img1 = Image("Icons/item_008" .. aa .. ".png", Rect(panel.width/2-15, 10, 30, 30))
    text2 = Text(aa, Rect(panel.width/2, 40, 100, 50))
    panel.AddChild(text1)
    panel.AddChild(text2)
    panel.AddChild(img1)

    button1 = Button("", Rect(Client.width/2+130, 250, 30, 30)) -- 버튼을 생성함.
    text3 = Text("Yes", Rect(0, 0, 30, 30))
    button1.AddChild(text3) -- 버튼에 이미지를 띄움.
    button1.onClick.Add(function() -- 버튼을 클릭 시
    rr = math.random(1, 99)  --1 이상 6 이하인 정수 생성
    print("DICE : " .. rr)
    panel.Destroy()
    button1.Destroy()
    button2.Destroy()
    return rr
    end)

    button2 = Button("", Rect(Client.width/2+240, 250, 30, 30)) -- 버튼을 생성함.
    text4 = Text("No", Rect(0, 0, 30, 30))
    button2.AddChild(text4) -- 버튼에 이미지를 띄움.
    button2.onClick.Add(function() -- 버튼을 클릭 시
    rr = 0
    print("DICE : " .. rr)
    panel.Destroy()
    button1.Destroy()
    button2.Destroy()
    return rr
    end)

end