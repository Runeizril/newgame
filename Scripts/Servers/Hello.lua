﻿itemAllCnt = 3
itemMaxIdx = itemAllCnt - 1
paramSplitDiv = "||"

function onUnitDead(target, attacker) --target은 죽은자, attacker는 공격자
    
    if (target.type==2 and attacker.type==0) then-- target monster
            local itemNum = math.random(1, itemMaxIdx) -- 처형자의 주화 빼고 득
                     
            if (attacker.partyID ~= 0) then
                local partyPlayers = attacker.party.players
                local ranUserNum = math.random(1, #partyPlayers)
                local getItemUser = partyPlayers[ranUserNum].unit
                getItemUser.AddItem(itemNum)
		if(itemNum~=0) then
			getItemUser.say("내가 " .. Server.GetItem(itemNum).name .. " 획득 했지롱~~")
		end

                for i = 1, #partyPlayers do
                    if(ranUserNum ~= ranUserNum) then
                        partyPlayers[ranUserNum].unit.AddItem(0)
                    end
                end
            else
                attacker.AddItem(itemNum) --공격자에게 확률로 0번~2번 중랜덤아이템을 준다. 
            end
	        -- target.RemoveItem(000) --죽은자에게 확률로 0번 아이템을 뺏긴다.
    end
end
Server.onUnitDead.Add(onUnitDead) -- Server.onUnitDead에 onUnitDead함수를 추가한다.

-- common
