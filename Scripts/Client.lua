function SplitString(str, pat)
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
     last_end = e+1
     s, e, cap = str:find(fpat, last_end)
   end

   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function widgetText(x, y)
	return Text("",Rect(Client.width-x, y, 200, 50))
end

pt = {};
pt[1] = {}; pt[2] = {}; pt[3] = {}; pt[4] = {};

--텍스트 출력 좌표는 알아서 수정할것.
pt[1][1] = widgetText(545,-10);
pt[1][1].color  = Color(255,255,255);
pt[1][2] = widgetText(545,5);
pt[1][2].color  = Color(255,0,0);
pt[1][3] = widgetText(545,20);
pt[1][3].color  = Color(0,0,255);

pt[2][1] = widgetText(465,-10);
pt[2][1].color  = Color(255,255,255);
pt[2][2] = widgetText(465,5);
pt[2][2].color  = Color(255,0,0);
pt[2][3] = widgetText(465,20);
pt[2][3].color  = Color(0,0,255);

pt[3][1] = widgetText(385,-10);
pt[3][1].color  = Color(255,255,255);
pt[3][2] = widgetText(385,5);
pt[3][2].color  = Color(255,0,0);
pt[3][3] = widgetText(385,20);
pt[3][3].color  = Color(0,0,255);

pt[4][1] = widgetText(305,-10);
pt[4][1].color  = Color(255,255,255);
pt[4][2] = widgetText(305,5);
pt[4][2].color  = Color(255,0,0);
pt[4][3] = widgetText(305,20);
pt[4][3].color  = Color(0,0,255);

Client.GetTopic("party").Add(
  function(unitData)
    datas = SplitString(unitData,"\\");
      for i, v in ipairs(datas) do
        mem = SplitString(v,",");
	for j, t in ipairs(mem) do
	  pt[i][j].text = t;
	end
      end 
  end
)
 --test